DrosalysWebWebSocketBundle
=================

Include WebSocket into your Symfony app.

## Install

```
composer require drosalys-web/web-socket-bundle
```

### Symfony integration (^4.4|^5.0)

Flex not yet available.

- Enable the bundle in your `config/bundle.php`:
```php
<?php

return [
    //...
    DrosalysWeb\Bundle\WebSocketBundle\DrosalysWebWebSocketBundle::class => ['all' => true],
    //...
];
```

- That all for default configuration.

## Documentations

TODO

## License

This bundle is under the MIT license. See the complete license:

    LICENSE
