<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Command;

use DrosalysWeb\Bundle\WebSocketBundle\Monitoring\MonitoringRegister;
use DrosalysWeb\Bundle\WebSocketBundle\Server\IoServer as DrosalysIoServer;
use Psr\Log\LoggerInterface;
use Ratchet\Server\IoServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class ServerRunCommand
 *
 * @author Benjamin Georgeault
 */
class ServerRunCommand extends Command
{
    protected static $defaultName = 'drosalys:ws:server-run';

    private IoServer $ioServer;
    private string $serverUri;
    private ?MonitoringRegister $monitoring;
    private ?LoggerInterface $logger;

    public function __construct(IoServer $ioServer, string $serverUri, ?MonitoringRegister $monitoring = null, ?LoggerInterface $logger = null)
    {
        parent::__construct();
        $this->ioServer = $ioServer;
        $this->serverUri = $serverUri;
        $this->monitoring = $monitoring;
        $this->logger = $logger;
    }

    protected function configure()
    {
        $this
            ->setDescription('Run the Web Socket server.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $output->writeln(sprintf('Web Socket server listening on <info>%s</info>.', $this->serverUri));

        if ($this->ioServer instanceof DrosalysIoServer) {
            $this->ioServer
                ->setOutput($output)
                ->setApplication($this->getApplication())
                ->setLogger($this->logger)
            ;
        }

        try {
            $this->ioServer->run();
        } catch (\Throwable $e) {
            if (null !== $this->monitoring) {
                $this->monitoring->catchException($e);
            }

            $io->error([
                sprintf('Exception "%s" happen in file "%s" at line %d.', get_class($e), $e->getFile(), $e->getLine()),
                $e->getMessage(),
            ]);

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
