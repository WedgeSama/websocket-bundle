<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Component;

use Psr\Http\Message\RequestInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServerInterface;

/**
 * Class AbstractHttpComponent
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractHttpComponent extends AbstractMessageComponent implements HttpServerInterface
{
    /**
     * @inheritDoc
     */
    public function onOpen(ConnectionInterface $conn, RequestInterface $request = null)
    {
    }
}
