<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Component;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;

/**
 * Class AbstractMessageComponent
 *
 * @author Benjamin Georgeault
 */
abstract class AbstractMessageComponent extends AbstractComponent implements MessageComponentInterface
{
    /**
     * @inheritDoc
     */
    public function onMessage(ConnectionInterface $conn, $msg)
    {
    }
}
