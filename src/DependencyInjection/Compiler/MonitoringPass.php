<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class MonitoringPass
 *
 * @author Benjamin Georgeault
 */
class MonitoringPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('drosalys.web_socket.monitoring.register')) {
            return;
        }

        $monitoringReferences = [];
        foreach ($container->findTaggedServiceIds('drosalys.ws.monitoring') as $id => $tags) {
            $monitoringReferences[] = new Reference($id);
        }

        $container->getDefinition('drosalys.web_socket.monitoring.register')
            ->setArguments([
                $monitoringReferences,
            ])
        ;
    }
}
