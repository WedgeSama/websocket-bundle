<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault <https://www.drosalys.fr/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle;

use DrosalysWeb\Bundle\WebSocketBundle\DependencyInjection\Compiler\MonitoringPass;
use DrosalysWeb\Bundle\WebSocketBundle\DependencyInjection\DrosalysWebWebSocketExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\Routing\DependencyInjection\RoutingResolverPass;

/**
 * Class DrosalysWebWebSocketBundle
 *
 * @author Benjamin Georgeault
 */
class DrosalysWebWebSocketBundle extends Bundle
{
    /**
     * @inheritDoc
     */
    public function getContainerExtension()
    {
        return new DrosalysWebWebSocketExtension();
    }

    /**
     * @inheritDoc
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new MonitoringPass());
        $container->addCompilerPass(new RoutingResolverPass(
            'drosalys.web_socket.routing.resolver',
            'drosalys.ws.routing.loader'
        ));
    }
}
