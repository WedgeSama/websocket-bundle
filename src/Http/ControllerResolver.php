<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Http;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Ratchet\ComponentInterface;
use Ratchet\Http\HttpServerInterface;
use Ratchet\MessageComponentInterface;
use Ratchet\Wamp\WampServer;
use Ratchet\Wamp\WampServerInterface;
use Ratchet\WebSocket\MessageComponentInterface as WsMessageComponentInterface;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\LoopInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as SymfonyContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;

/**
 * Class ControllerResolver
 *
 * @author Benjamin Georgeault
 */
class ControllerResolver implements ControllerResolverInterface
{
    private ContainerInterface $container;
    private LoopInterface $loop;
    private ?LoggerInterface $logger;

    public function __construct(ContainerInterface $container, LoopInterface $loop, LoggerInterface $logger = null)
    {
        $this->container = $container;
        $this->loop = $loop;
        $this->logger = $logger;
    }

    /**
     * @return HttpServerInterface|false
     */
    public function getController(Request $request)
    {
        if (!$controller = $request->attributes->get('_controller')) {
            if (null !== $this->logger) {
                $this->logger->warning('Unable to look for the controller as the "_controller" parameter is missing.');
            }

            return false;
        }

        if (\is_object($controller)) {
            $this->validController($request, $controller);
            return $this->decorateController($controller);
        }

        try {
            $controller = $this->createController($controller);
        } catch (\InvalidArgumentException $e) {
            throw new \InvalidArgumentException(sprintf(
                'The controller for URI "%s" cannot be found.',
                $request->getPathInfo()
            ), 0, $e);
        }

        $this->validController($request, $controller);

        return $this->decorateController($controller);
    }

    private function validController(Request $request, $controller): void
    {
        if (!($controller instanceof ComponentInterface)) {
            throw new \InvalidArgumentException(sprintf(
                'The controller for URI "%s" should implement "%s" interface.',
                $request->getPathInfo(),
                ComponentInterface::class
            ));
        }
    }

    private function createController($class)
    {
        $class = ltrim($class, '\\');

        if ($this->container->has($class)) {
            return $this->container->get($class);
        }

        if (class_exists($class)) {
            return new $class();
        }

        throw new \InvalidArgumentException(sprintf(
            'Class "%s" not found.',
            $class
        ));
    }

    private function decorateController(ComponentInterface $component): HttpServerInterface
    {
        if ($component instanceof ContainerAwareInterface && $this->container instanceof SymfonyContainerInterface) {
            $component->setContainer($this->container);
        }

        if ($component instanceof WampServerInterface) {
            $component = new WampServer($component);
        }

        if (
            $component instanceof WampServer ||
            $component instanceof MessageComponentInterface ||
            $component instanceof WsMessageComponentInterface
        ) {
            $component = new WsServer($component);
            $component->enableKeepAlive($this->loop);
        }

        return $component;
    }
}
