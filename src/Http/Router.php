<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Http;

use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Ratchet\ConnectionInterface;
use Ratchet\Http\CloseResponseTrait;
use Ratchet\Http\HttpServerInterface;
use Ratchet\Http\NoOpHttpServerController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class Router
 *
 * @author Benjamin Georgeault
 */
class Router implements HttpServerInterface, LoggerAwareInterface
{
    use CloseResponseTrait;
    use LoggerAwareTrait;

    private UrlMatcherInterface $matcher;
    private ControllerResolver $controllerResolver;
    private TokenStorageInterface $tokenStorage;
    private UserProviderInterface $userProvider;
    /**
     * @var string[]
     */
    private array $firewalls;
    private HttpServerInterface $defaultController;

    public function __construct(
        UrlMatcherInterface $wsMatcher,
        ControllerResolver $controllerResolver,
        TokenStorageInterface $tokenStorage,
        UserProviderInterface $userProvider,
        array $firewalls = [],
        ?HttpServerInterface $defaultController = null
    ) {
        $this->matcher = $wsMatcher;
        $this->controllerResolver = $controllerResolver;
        $this->tokenStorage = $tokenStorage;
        $this->userProvider = $userProvider;
        $this->firewalls = $firewalls;
        $this->defaultController = $defaultController ?? new NoOpHttpServerController();
    }

    /**
     * {@inheritdoc}
     * @throws \UnexpectedValueException If a controller is not \Ratchet\Http\HttpServerInterface
     */
    public function onOpen(ConnectionInterface $conn, RequestInterface $request = null)
    {
        if (null === $request) {
            throw new \UnexpectedValueException('$request can not be null');
        }

        $uri = $request->getUri();
        $context = $this->matcher->getContext();
        $context->setMethod($request->getMethod());
        $context->setHost($uri->getHost());

        try {
            $route = $this->matcher->match($uri->getPath());
        } catch (MethodNotAllowedException $e) {
            $this->close($conn, 405, [
                'Allow' => $e->getAllowedMethods(),
            ]);
            return;
        } catch (ResourceNotFoundException $e) {
            $this->close($conn, 404);
            return;
        }

        $controller = $this->controllerResolver->getController($this->convertPsrToSymfonyRequest($request, $route));

        if (!($controller instanceof HttpServerInterface)) {
            throw new \UnexpectedValueException(sprintf(
                'All routes class must implement "%s".',
                HttpServerInterface::class
            ));
        }

        $this->refreshToken($conn);
        $conn->controller = $controller;
        $controller->onOpen($conn, $request);
    }

    /**
     * @inheritDoc
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        if (isset($from->controller)) {
            $this->refreshToken($from);
            $from->controller->onMessage($from, $msg);
        }
    }

    /**
     * @inheritDoc
     */
    public function onClose(ConnectionInterface $conn)
    {
        if (isset($conn->controller)) {
            $this->refreshToken($conn);
            $conn->controller->onClose($conn);
            $conn->controller = null; // Prevent memory leak.
        }

        $conn->close();
    }

    /**
     * @inheritDoc
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        if (isset($conn->controller)) {
            $this->refreshToken($conn);
            $conn->controller->onError($conn, $e);
        }
    }

    private function convertPsrToSymfonyRequest(RequestInterface $psrRequest, array $attributes = []): Request
    {
        $request = Request::create($psrRequest->getUri(), 'GET', [], [], [], [], $psrRequest->getBody());
        $request->headers->replace($psrRequest->getHeaders());
        $request->attributes->add($attributes);

        return $request;
    }

    private function refreshToken(ConnectionInterface $conn): void
    {
        /** @var Session $session */
        if (isset($conn->Session) && null !== $session = $conn->Session) {
            $conn->token = $this->getToken($session);

            if (
                (null !== $this->logger) &&
                (null !== $conn->token) &&
                (null !== $user = $conn->token->getUser())
            ) {
                $str = 'Loaded token with User.';

                if ($user instanceof UserInterface) {
                    $str .= sprintf(' Username "%s".', $user->getUsername());

                    if (method_exists($user, 'getId')) {
                        $str .= sprintf(' ID "%s".', $user->getId());
                    }
                } else {
                    $str .= ' ' . $user;
                }

                $this->logger->info($str);
            }

        } else {
            unset($conn->token);
        }

        $this->applyToken($conn);
    }

    private function getToken(Session $session): ?TokenInterface
    {
        foreach ($this->firewalls as $firewall) {
            if (null !== $serializedToken = $session->get('_security_'.$firewall)) {
                /** @var TokenInterface $token */
                $token = unserialize($serializedToken);

                if (
                    (null !== $user = $token->getUser()) &&
                    \is_object($user)
                ) {
                    $token->setUser($this->userProvider->refreshUser($user));
                }

                return $token;
            }
        }

        return null;
    }

    private function applyToken(ConnectionInterface $conn): void
    {
        /** @var TokenInterface $token */
        if (isset($conn->token) && null !== $token = $conn->token) {
            $this->tokenStorage->setToken($token);
        } else {
            $this->tokenStorage->setToken(null);
        }
    }
}
