<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Monitoring;

/**
 * Interface MonitoringInterface
 *
 * @author Benjamin Georgeault
 */
interface MonitoringInterface
{
    public function catchException(\Throwable $e, \DateTimeInterface $throwAt): void;
}
