<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Monitoring;

/**
 * Class MonitoringRegister
 *
 * @author Benjamin Georgeault
 */
final class MonitoringRegister
{
    /**
     * @var MonitoringInterface[]
     */
    private iterable $monitoringArray;

    /**
     * MonitoringRegister constructor.
     * @param MonitoringInterface[] $monitoringArray
     */
    public function __construct(iterable $monitoringArray)
    {
        $this->monitoringArray = $monitoringArray;
    }

    public function catchException(\Throwable $e): void
    {
        $date = new \DateTimeImmutable();

        foreach ($this->monitoringArray as $monitoring) {
            $monitoring->catchException($e, $date);
        }
    }
}
