<?php

/*
 * This file is part of the websocket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Routing\Generator;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RequestContext;

/**
 * Class WebSocketWrapperUrlGenerator
 *
 * @author Benjamin Georgeault
 */
class WebSocketWrapperUrlGenerator implements UrlGeneratorInterface
{
    private UrlGeneratorInterface $wrappedGenerator;

    public function __construct(UrlGeneratorInterface $wrappedGenerator)
    {
        $this->wrappedGenerator = $wrappedGenerator;
    }

    /**
     * @inheritDoc
     */
    public function setContext(RequestContext $context)
    {
        $this->wrappedGenerator->setContext($context);
    }

    /**
     * @inheritDoc
     */
    public function getContext(): RequestContext
    {
        return $this->wrappedGenerator->getContext();
    }

    /**
     * @inheritDoc
     */
    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH): string
    {
        return preg_replace(
            '/^http(s?):\/\//',
            'ws$1://',
            $this->wrappedGenerator->generate($name, $parameters, $referenceType)
        );
    }
}
