<?php

/*
 * This file is part of the web-socket-bundle package.
 *
 * (c) Benjamin Georgeault
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace DrosalysWeb\Bundle\WebSocketBundle\Routing\Loader;

use Symfony\Component\Routing\Loader\YamlFileLoader as BaseLoader;

/**
 * Class YamlFileLoader
 *
 * @author Benjamin Georgeault
 */
class YamlFileLoader extends BaseLoader
{
    /**
     * @var string[]
     */
    private static array $disabledKeys = [
        'host', 'schemes', 'methods', 'condition',
    ];

    /**
     * @inheritDoc
     */
    protected function validate($config, $name, $path)
    {
        parent::validate($config, $name, $path);

        if ($extraKeys = array_intersect(array_keys($config), self::$disabledKeys)) {
            throw new \InvalidArgumentException(sprintf(
                'The routing file "%s" contains disabled keys for "%s": "%s". Disabled ones are: "%s".',
                $path,
                $name,
                implode('", "', $extraKeys),
                implode('", "', self::$disabledKeys)
            ));
        }
    }
}
